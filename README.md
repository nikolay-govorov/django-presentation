## To show a presentation using django

### Install dependencies

```bash
$ pip install -r requirements.txt
```

### Run development server

```bash
$ python manage.py runserver
```
